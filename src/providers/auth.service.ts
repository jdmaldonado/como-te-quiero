import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import firebase from 'firebase';


@Injectable()
export class AuthService {

    constructor(public af: AngularFire) {
    }

    createAuthUser(_email: string, _password: string): firebase.Promise<any> {
        return this.af.auth.createUser({email: _email, password: _password });
    };

    loginWithPassword(_email: string, _password: string): firebase.Promise<any> {
        return this.af.auth.login({email: _email, password: _password })
    };

    resetPassword(email: string): firebase.Promise<any> {
        return firebase.auth().sendPasswordResetEmail(email);
    };

    logout(): void {
        this.af.auth.logout();
    };

}