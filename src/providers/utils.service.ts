import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';

@Injectable()
export class UtilsService {

    public loading : any;
    public toast : any;

    constructor(
        private loadingCtrl: LoadingController,
        private toastCtrl: ToastController
    ) { }

    showLoading( text: string = null ) {
        let message = (text)? text : 'Please wait ...';

        this.loading = this.loadingCtrl.create({
            content: message
        });
        this.loading.present();
    }

    hideLoading() {
        this.loading.dismiss();
    }

    showMessage( message: string, time: number = null ) {
        if(time){
            this.toast = this.toastCtrl.create({
                message: message,
                duration: time
            });
        }
        else{
            this.toast = this.toastCtrl.create({
                message: message
            });
        };

        this.toast.present();
    }

    hideMessage() {
        this.toast.dismiss();
    }

}