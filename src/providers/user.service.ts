import { Injectable } from '@angular/core';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';

@Injectable()
export class UserService {

    loggedUser: FirebaseObjectObservable<any>;

    constructor(public af: AngularFire) {}

    getLoggedUser(){
		return this.loggedUser;
    };

    setLoggedUser() {
		this.af.auth.subscribe( auth => {
            if(auth){
                this.loggedUser = this.getUserById(auth.uid);
            }
        })
	};

    // Transactions
    createUser(userId, userInfo){
        const currentUser = this.af.database.object('/users/'+userId);
        return currentUser.set(userInfo);
    };

    getUserById(userId){
        return this.af.database.object('/users/'+userId);
    };

    
}