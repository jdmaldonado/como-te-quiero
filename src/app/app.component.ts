import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFire } from 'angularfire2';

import { UserService } from '../providers/user.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: string = "Home";

  constructor(
    af: AngularFire,
    platform: Platform,
    statusBar: StatusBar,
    userService: UserService,
    splashScreen: SplashScreen) {

    af.auth.subscribe(user => {
      if (user) {
        userService.setLoggedUser();
        this.rootPage = "Home";
      } else {
        this.rootPage = "Login";
      }
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

