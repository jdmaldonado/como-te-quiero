import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

import { MyApp } from './app.component';

import { AuthService } from '../providers/auth.service';
import { UserService } from '../providers/user.service';
import { UtilsService } from '../providers/utils.service';

export const firebaseConfig = {
    apiKey: "AIzaSyD8QbJsY1puKN6v6Bcp_G0v9jSIKtvoIzU",
    authDomain: "comotequiero-b8817.firebaseapp.com",
    databaseURL: "https://comotequiero-b8817.firebaseio.com",
    projectId: "comotequiero-b8817",
    storageBucket: "comotequiero-b8817.appspot.com",
    messagingSenderId: "348053566297"
};

const firebaseAuthConfig = {
    provider: AuthProviders.Password,
    method: AuthMethods.Password
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig, firebaseAuthConfig),
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    AuthService,
    UserService,
    UtilsService,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
