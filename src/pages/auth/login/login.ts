import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { AuthService } from '../../../providers/auth.service';
import { UtilsService } from '../../../providers/utils.service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class Login {

  form : any;

  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    private utils: UtilsService
    ) {
      this.form = {
        email: '',
        password: ''
      }
  }

  login() {
      this.utils.showLoading();

      this.authService.loginWithPassword(this.form.email, this.form.password)
      .then( response => {
        this.navCtrl.setRoot("Home");
        this.utils.hideLoading();
      })
      .catch( error => {
        console.error(error);
        this.utils.showMessage( error.message, 5000 );
        this.utils.hideLoading();
      });
  }

  goToRegister(){
    this.navCtrl.push("Register");
  }

  
}